﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsGame.Enums
{
    public enum TypeOfGame
    {
        PlayerVSComputer,
        ComputerVSComputer,
        DifferentGameEachTime
    }
}
