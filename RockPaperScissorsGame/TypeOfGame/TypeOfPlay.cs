﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RockPaperScissorsGame.Enums;

namespace RockPaperScissorsGame.TypeOfGame
{
    public class TypeOfPlay : ITypeOfPlay
    {
        public Enums.TypeOfGame GetTypeOfGame(string typeOfPlay)
        {
            if (typeOfPlay == "ComputerVSComputer")
                return Enums.TypeOfGame.ComputerVSComputer;
                       
            return Enums.TypeOfGame.PlayerVSComputer;
        }
    }
}
