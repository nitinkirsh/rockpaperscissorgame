﻿namespace RockPaperScissorsGame.TypeOfGame
{
    public interface ITypeOfPlay
    {
        Enums.TypeOfGame GetTypeOfGame(string TypeOfPlay);
    }
}
