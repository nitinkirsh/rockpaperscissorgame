﻿using RockPaperScissorsGame.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsGame.ResultGenerator
{
  public  interface IDecisionGenerator
    {

         Result GenerateResult(Move computerMove, Move playerMove);
    }
}
