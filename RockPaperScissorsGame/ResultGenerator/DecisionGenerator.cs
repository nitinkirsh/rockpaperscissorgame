﻿using System;
using RockPaperScissorsGame.Enums;

namespace RockPaperScissorsGame.ResultGenerator
{
    public class DecisionGenerator: IDecisionGenerator
    {
        public Result GenerateResult(Move computerMove, Move playerMove)
        {

            if (CheckIfPalyerWin(computerMove, playerMove))
                return Result.PlayerWins;
            else if (computerMove==playerMove)
                return Result.Draw;
            else
                return Result.ComputerWins;
            
        }

        private bool CheckIfPalyerWin(Move computerMove, Move playerMove)
        {
            return (playerMove == Move.Paper && computerMove == Move.Rock)
                 || (playerMove == Move.Rock && computerMove == Move.Scissors)
                 || (playerMove == Move.Scissors && computerMove == Move.Paper);
            
        }
    }
}