﻿using RockPaperScissorsGame.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsGame.Games
{
    public class ComputerMove : IComputerMove
    {
       private  Random _random;
     
        public Move GenerateComputerMove()
        {
            int num = GetRandomNmber();

            if (num == 100)
                return  Move.Paper;
            else if(num==101)
                return  Move.Scissors;
            else
                return  Move.Rock;
           
        }


        public int GetRandomNmber()
        {
            _random = new Random();
            return _random.Next(99, 102);
        }
    }
}
