﻿using RockPaperScissorsGame.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RockPaperScissorsGame.Games
{
   public interface IComputerMove
    {
       Move GenerateComputerMove();
        int GetRandomNmber();
    }
}
