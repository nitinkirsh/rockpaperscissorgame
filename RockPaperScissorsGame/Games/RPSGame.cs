﻿using RockPaperScissorsGame.Enums;
using System;
using RockPaperScissorsGame.ResultGenerator;
using RockPaperScissorsGame.TypeOfGame;

namespace RockPaperScissorsGame.Games
{
    public class RPSGame
    {
        private int _playerMove;
        private string _gameType;
        private IDecisionGenerator _decisionGenerator;
        private IComputerMove _computerMove;
        private ITypeOfPlay _typeofPlay; 


        public RPSGame()
        {
            _decisionGenerator = new DecisionGenerator();
            _computerMove = new ComputerMove();
            _typeofPlay = new  TypeOfPlay();
        }

        public RPSGame(IDecisionGenerator decisionGenerator,IComputerMove computerMove,
            ITypeOfPlay typeOfPlay)
        {
            _decisionGenerator = decisionGenerator;
            _computerMove = computerMove;
            _typeofPlay = typeOfPlay;
        }

      

        public int PlayerMove
        {
            private get
            {
              //  if (String.IsNullOrEmpty(_playerMove))
                  //  throw new ArgumentNullException("PlayerMove");
                return _playerMove;
            }
            set
            {
                _playerMove = value;
            }
        }


        public string GameType
        {
            private get
            {
                if (String.IsNullOrEmpty(_gameType))
                    throw new ArgumentNullException("_gameType");
                return _gameType;
            }
            set
            {
                _gameType = value;
            }
        }



        public Result Result()
        {
            var decisionGenerator = _decisionGenerator;

          return
                  _typeofPlay.GetTypeOfGame(GameType) == Enums.TypeOfGame.PlayerVSComputer ?

                decisionGenerator.GenerateResult(_computerMove.GenerateComputerMove(),
               
              (Move) PlayerMove)
                :
                  decisionGenerator.GenerateResult(_computerMove.GenerateComputerMove(),
                _computerMove.GenerateComputerMove())
                ;

           
        }

    }
}


