﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RockPaperScissorsGame.Games;
using Moq;
using RockPaperScissorsGame.ResultGenerator;
using RockPaperScissorsGame.Enums;
using RockPaperScissorsGame.TypeOfGame;

namespace RockPaperScissorsGameTest
{
    [TestClass]
    public class RPSGameTest
    {
        [TestMethod]
        [Ignore]
        public void ComputerRock_PlayerPaper_PlayerWins()
        {
            var game = new RPSGame();
            game.PlayerMove = 0;
            game.GameType = "PlayerVSComputer";
            Mock<IComputerMove> mockComputerMove = new Mock<IComputerMove>();
            Mock<IDecisionGenerator> mockDecisionGenerator = new Mock<IDecisionGenerator>();
            var mockTypeofGame = new Mock<ITypeOfPlay>();
            
            mockTypeofGame.Setup(x => x.GetTypeOfGame("PlayerVSComputer"))
                .Returns(TypeOfGame.PlayerVSComputer);

            mockComputerMove.Setup(r => r.GetRandomNmber()).Returns(102);
                     
            mockDecisionGenerator.Setup(
              x => x.GenerateResult(Move.Paper, Move.Rock))
              .Returns((Result.PlayerWins));


            Assert.AreEqual(Result.PlayerWins, game.Result());
        }

        [TestMethod]
        [Ignore]
        public void ComputerRock_PlayerScissors_ComputerWins()
        {
            var game = new RPSGame();
            game.PlayerMove = 2;
            game.GameType = "PlayerVSComputer";
           var mockComputerMove = new Mock<IComputerMove>();
           var mockDecisionGenerator = new Mock<IDecisionGenerator>();

            var mockTypeofGame = new Mock<ITypeOfPlay>();

            mockTypeofGame.Setup(x => x.GetTypeOfGame("PlayerVSComputer"))
                .Returns(TypeOfGame.PlayerVSComputer);


            mockComputerMove.Setup(r => r.GetRandomNmber()).Returns(103);

            mockDecisionGenerator.Setup(
              x => x.GenerateResult(Move.Paper, Move.Rock))
              .Returns((Result.PlayerWins));

            Assert.AreEqual(Result.ComputerWins, game.Result());
        }


        [TestMethod]
        [Ignore]
        public void ComputerRock_PlayerRock_Draw()
        {
            var game = new RPSGame();
            game.PlayerMove = 2;
            game.GameType = "PlayerVSComputer";
           var mockComputerMove = new Mock<IComputerMove>();
           var mockDecisionGenerator = new Mock<IDecisionGenerator>();

            var mockTypeofGame = new Mock<ITypeOfPlay>();

            mockTypeofGame.Setup(x => x.GetTypeOfGame("PlayerVSComputer"))
                .Returns(TypeOfGame.PlayerVSComputer);


            mockComputerMove.Setup(r => r.GetRandomNmber()).Returns(100);

            mockDecisionGenerator.Setup(
                x => x.GenerateResult(Move.Scissors, Move.Scissors))
                .Returns((Result.Draw));
            
           
            Assert.AreEqual(Result.Draw, game.Result());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ThrowsErrorIfPlayerMoveIsNotSet()
        {
            var game = new RPSGame();
            game.PlayerMove = 0;
            game.Result();
        }

       
    }
}
