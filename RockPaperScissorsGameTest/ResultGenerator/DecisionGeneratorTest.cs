﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using RockPaperScissorsGame.Enums;
using RockPaperScissorsGame.ResultGenerator;

namespace RockPaperScissorsGameTest
{
    /// <summary>
    /// Summary description for DecisionGeneratorTest
    /// </summary>
    [TestClass]
    public class DecisionGeneratorTest
    {
       
        [TestMethod]
        public void ComputerRock_PlayerPaper_ComputerWins()
        {
            var engine = new DecisionGenerator();
            var gameResult = engine.GenerateResult(Move.Rock, Move.Paper);
            Assert.AreEqual(Result.PlayerWins, gameResult);
        }

        [TestMethod]
        public void ComputerRock_PlayerScissors_ComputerWins()
        {
            var engine = new DecisionGenerator();
            var gameResult = engine.GenerateResult(Move.Rock, Move.Scissors);
            Assert.AreEqual(Result.ComputerWins, gameResult);
        }
    }
}
